const toDos = [
  { id: 1, description: 'Buy milk.' },
  { id: 2, description: 'Walk the dog.' },
  { id: 3, description: 'Wash the car.' },
  { id: 4, description: 'Workout.' },
  { id: 5, description: 'Make dinner reservations.' },
  { id: 6, description: 'Cook dinner.' },
  { id: 7, description: 'Pack a lunch.' },
  { id: 8, description: 'Give the dog a bath.' },
  { id: 9, description: 'Change the oil.' },
  { id: 10, description: 'Get cash from the ATM.' },
];

module.exports = {
  toDos,
};
