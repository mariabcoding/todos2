
# ToDos Node/Express API

## Running the Application Locally

### Install Dependencies

To install the project's dependencies, run the following command from the root of the project (the folder that contains the `package.json` file):

```
npm install
```

### Configuring Environment Variables

Add a file named `.env` to the root of the project and copy the contents of the `.env.example` file into it:

```
PORT=8080
```

_In most cases, you can leave these environment variable values as they are, but feel free to change any of them as needed so that the application can run locally on your machine._

### Run the Application

Now you're ready to run the application:

```
npm start
```

You'll see the following output after the application has successfully started and tested the connection to the database:

```
Listening on port 8080...
```

### Manually Testing the Application

Using the Visual Studio Code [REST Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client) extension, you can open `http/requests.http` file and send HTTP requests to the application.
