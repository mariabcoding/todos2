const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const { environment } = require('./config');
let { toDos } = require('./data');

const app = express();

app.use(express.json());
app.use(morgan('dev'));
app.use(cors());

app.get('/', (req, res) => {
  res.json({ message: 'Welcome to the ToDos API!' });
});

let nextToDoId = 11;

const validateToDo = (toDo) => {
  const errors = [];

  if (!toDo) {
    errors.push('Please provide a TODO.');
    return errors;
  }

  if (!toDo.description) {
    errors.push('Please provide a description.');
  }

  return errors;
};

app.get('/api/todos', (req, res) => {
  res.json(toDos);
});

app.get('/api/todos/:id(\\d+)', (req, res) => {
  const toDoId = parseInt(req.params.id, 10);
  const toDo = toDos.find((t) => t.id === toDoId);
  if (toDo) {
    res.json(toDo);
  } else {
    res.status(404).end();
  }
});

app.post('/api/todos', (req, res) => {
  const toDo = req.body;
  const errors = validateToDo(toDo);
  if (errors.length > 0) {
    res.status(400).json(errors);
  } else {
    toDo.id = nextToDoId;
    nextToDoId += 1;
    toDos.push(toDo);
    res.status(201).location(`/api/todos/${toDo.id}`).json(toDo);
  }
});

app.put('/api/todos/:id(\\d+)', (req, res) => {
  const toDoId = parseInt(req.params.id, 10);
  if (toDos.some((t) => t.id === toDoId)) {
    const toDo = req.body;
    const errors = validateToDo(toDo);
    if (errors.length > 0) {
      res.status(400).json(errors);
    } else if (toDoId !== toDo.id) {
      res.status(409).end();
    } else {
      const toDoToUpdate = toDos.find((t) => t.id === toDoId);
      toDoToUpdate.description = toDo.description;
      res.status(204).end();
    }
  } else {
    res.status(404).end();
  }
});

app.delete('/api/todos/:id(\\d+)', (req, res) => {
  const toDoId = parseInt(req.params.id, 10);
  if (toDos.some((t) => t.id === toDoId)) {
    toDos = toDos.filter((t) => t.id !== toDoId);
    res.status(204).end();
  } else {
    res.status(404).end();
  }
});

// Catch unhandled requests and forward to error handler.
app.use((req, res, next) => {
  const err = new Error('The requested resource couldn\'t be found.');
  err.status = 404;
  next(err);
});

// Generic error handler.
app.use((err, req, res, next) => {
  console.error(err);
  res.status(err.status || 500);
  const isProduction = environment === 'production';
  res.json({
    title: err.title || 'Server Error',
    message: err.message,
    stack: isProduction ? null : err.stack,
  });
});

module.exports = app;
