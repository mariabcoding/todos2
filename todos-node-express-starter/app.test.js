const request = require('supertest');
const { response } = require('./app.js');
const app = require('./app.js')

// why doesn't this work?

// it('get welcome message', async ()=>{
//   const res = await request(app)
//   .get('/')
//   .expect(reponse.message).toContain('Welcome to the ToDos API!' )
//   .expect(response.statusCode).toEqual(200)
//   .done()
// })

test('/ get', async () => {
  await request(app).get("/")
    .expect(200)
    .then((response) => {
     expect(response.text).toContain('Welcome to the ToDos API!')
    })
});

// how to test error?
test("invalid endpoint", async () => {
  await request(app).get("/abc123")
    .expect(404) 
    // .then((response) => {
    //   expect(response.text).toContain("The requested resource couldn't be found.")
    //  })
});

test('should return 10 toDos', async ()=>{
  await request(app).get('/api/todos')
  .expect(200)
  .then((response) => {
   expect(response.body.length).toBe(10)
  })
});

test ('should return toDo #1', async () => {
  await request(app).get('/api/todos/1')
  .expect(200)
  .then((response) => {
   expect(response.body.description).toEqual('Buy milk.')
  })

})

test ('should notFind Invalid toDo', async () => {
  await request(app).get('/api/todos/999')
  .expect(404)
})

test ('should add valid todo', async () => {
  const newToDo = { description: 'Eat Cake' }

  await request(app).post('/api/todos')
    .send(newToDo)
    .expect(201)
    .then((response) => {
      expect(response.body.description).toEqual('Eat Cake')
    })

})

test ('should not add invalid toDo', async () => { 
  const invalidTodo = {id: 999, beavers: 5, weasels: 4}
  await request(app).post('/api/todos')
  .send(null)
  .expect(400)
})

test ('should update toDo', async ()=> {
  const updatedTodo = { id: 6, description: 'Go out for dinner.' }
  await request(app).put("/api/todos/6")
  .send(updatedTodo)
  .expect(204)
  // how do i check this with async code??
  // .then(await request(app).get("/api/todos/6")
  // .expect(200)
  // .then((response) => {
  //   expect(response.body.description).toBe('Go out for dinner.')
  // }),
  // expect.assertions(2))
})

test ('should delete todo', async ()=> {
  await request(app).delete("/api/todos/6")
  .expect(204)
})

test ('should not delete todo', async ()=> {
  await request(app).delete("/api/todos/999")
  .expect(404)
})


 

